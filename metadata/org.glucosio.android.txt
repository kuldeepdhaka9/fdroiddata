Disabled:A license (of the non-free variety) is required to distribute this. We don't have a license.
AntiFeatures:UpstreamNonFree
Categories:Sports & Health
License:GPLv3
Web Site:https://glucosio.org/
Source Code:https://github.com/Glucosio/android
Issue Tracker:https://github.com/Glucosio/android/issues

Auto Name:Glucosio
Summary:Manage your diabetes
Description:
Manage and research diabetes.
.

Repo Type:git
Repo:https://github.com/Glucosio/android

Build:1.0,1
    commit=3391956e77b391e4e759ad4b607dad7a3e68c8c6
    subdir=app
    gradle=yes

Build:0.8.1,2
    commit=540384a2acda7421a344f686258758c4898900d9
    subdir=app
    gradle=yes

Build:0.8.2,3
    disable=play-services
    commit=0.8.2
    subdir=app
    gradle=yes

Build:0.10.3,18
    disable=play-services
    commit=0.10.3
    subdir=app
    gradle=yes

Build:1.1.2-FOSS,33
    commit=18c02f5d51a305c744149f0f9428c273499861bb
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/google-services/d' build.gradle ../build.gradle && \
        sed -i -e 's|@integer/google_play_services_version|0|g' src/main/AndroidManifest.xml

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.1.2
Current Version Code:33
